var express     = require('express');
var bodyParser  = require('body-parser');
var config      = require('./config');

var app         = express();

app.use(require('./middlewares'));
app.use(bodyParser.json());

app.use('/', require('./routes/config'));

app.use(function(err, req, res, next){
    res.status(err.status || 500).end();
});

module.exports = app;

// If running as unit test, we return here so we don't start the server.
if (process.env.NODE_ENV === 'test') { return; }

require('./db')(config)
.once('open', function (){
    app.listen(config.port, function () {
        console.log('App listening on port ' + config.port);
    });
});
