var config = require('../config');
module.exports = function (req, res, next) {
    if (req.headers['x-authorization'] !== config.authorization) {
        return res.status(401).end();
    }
    next();
};
