var request     = require('supertest');
var server      = require('../server');

describe('authorization middleware', function () {
    describe('authorization header not passed', function () {
        it('should return unauthorized', function () {
            return request(server)
            .get('/')
            .expect(401);
        });
    });
});
