var mongoose = require('mongoose');
var when     = require('when');

module.exports = function(config) {
    mongoose.Promise = when.Promise;
    mongoose.connect(config.database);

    mongoose.connection.on('connected', function () {
        console.log('Mongoose default connection open to ' + config.database);
    });

    mongoose.connection.on('error',function (err) {
        console.log('Mongoose default connection error: ' + err);
    });

    mongoose.connection.on('disconnected', function () {
        console.log('Mongoose default connection disconnected');
    });

    process.on('SIGINT', function() {
        mongoose.connection.close(function () {
            console.log('Mongoose default connection disconnected through app termination');
            process.exit(0);
        });
    });

    return mongoose.connection;
};
