var request     = require('supertest');
var server      = require('./server');

describe('server', function () {
    describe('request to route that does not exist', function () {
        it('should return not found', function () {
            return request(server)
            .get('/')
            .set('X-Authorization', 'test')
            .expect(404);
        });
    });
});
