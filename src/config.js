module.exports = {
    secret          : 'YvAQUZ4Q(nA1Nr<=p',
    database        : process.env.DB_URI ||
                      ('mongodb://' + (process.env.DB_HOST || 'localhost') +
                        ':' + (process.env.DB_PORT || '27017') +
                        '/' + (process.env.DB_NAME || 'config')),
    authorization   : process.env.AUTHORIZATION || 'test',
    port            : process.env.PORT || 3000
};
