var request     = require('supertest');
var sinon       = require('sinon');

var Config      = require('../../models/config');
var server      = require('../../server');

var sandbox = sinon.sandbox.create();

describe('config', function () {
    before(function () {
        sandbox.stub(console, 'info');
        sandbox.stub(console, 'error');

        sandbox.stub(Config.prototype, 'validateSync', function () {
            return new Error();
        });
    });

    after(function () {
        sandbox.restore();
    });

    describe('create without a valid configuration', function () {
        it('should return bad request when missing entity_types', function () {
            return request(server)
            .post('/config/create')
            .send({
                name: 'test',
                token: 'test123456789',
                access: {
                    apps: ['desktop_app'],
                    contexts: ['work_out'],
                    entity_types: ''
                }
            })
            .set('X-Authorization', 'test')
            .expect(400);
        });

        it('should return bad request when contexts is missing or empty', function () {
            return request(server)
            .post('/config/create')
            .send({
                name: 'test',
                token: 'test123456789',
                access: {
                    apps: ['desktop_app'],
                    contexts: [''],
                    entity_types: '*'
                }
            })
            .set('X-Authorization', 'test')
            .expect(400);
        });

        it('should return bad request when apps is missing or empty', function () {
            return request(server)
            .post('/config/create')
            .send({
                name: 'test',
                token: 'test123456789',
                access: {
                    apps: [''],
                    contexts: ['work_out'],
                    entity_types: '*'
                }
            })
            .set('X-Authorization', 'test')
            .expect(400);
        });

        it('should return bad request when name is missing', function () {
            return request(server)
            .post('/config/create')
            .send({
                name: '',
                token: 'test123456789',
                access: {
                    apps: ['desktop_app'],
                    contexts: ['work_out'],
                    entity_types: '*'
                }
            })
            .set('X-Authorization', 'test')
            .expect(400);
        });

        it('should return bad request when token is missing', function () {
            return request(server)
            .post('/config/create')
            .send({
                name: 'test',
                token: '',
                access: {
                    apps: ['desktop_app'],
                    contexts: ['work_out'],
                    entity_types: '*'
                }
            })
            .set('X-Authorization', 'test')
            .expect(400);
        });
    });
});
