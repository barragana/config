var list = require('../../services/config/list');

module.exports = function (req, res, next) {
    console.info('[routes][config][list][info] - list configuration - query: %j', req.query);

    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);

    if (isNaN(limit) || isNaN(offset)) {
        console.info('[routes][config][list][error] - list configuration - error: request query error - query: %j', req.query);
        return next({ status: 400 });
    }

    list(offset, limit)
    .then(function (configsList) {
        console.info('[routes][config][list][success] - list configuration - configs list: %j', configsList);
        res.status(200).json(configsList);
    })
    .catch(function (error) {
        console.info('[routes][config][list][error] - list configuration - error: %j - error stack: %s', error, error.stack || 'no stack' );
        next(error);
    });
};
