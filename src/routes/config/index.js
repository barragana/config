var express = require('express');
var router  = express.Router();

router.post('/config/create'        , require('./create'));
router.get('/config/get/:id'        , require('./get'));
router.delete('/config/remove/:id'  , require('./remove'));
router.get('/config/list'           , require('./list'));

module.exports = router;
