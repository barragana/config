var remove = require('../../services/config/remove');

module.exports = function (req, res, next) {
    console.info('[routes][config][remove][info] - remove configuration - params: %j', req.params);

    remove(req.params.id)
    .then(function (result) {
        if (!result) {
            console.info('[routes][config][remove][error] - configuration not found - id: %s - delete: %j', req.params.id, result);
            return res.status(404).end();
        }

        console.info('[routes][config][remove][success] - remove configuration - id: %s - delete: %j', req.params.id, result);
        res.status(200).end();
    })
    .catch(function (error) {
        console.info('[routes][config][remove][error] - remove configuration - error: %j - error stack: %s', error, error.stack || 'no stack' );
        next(error);
    });
};
