var get = require('../../services/config/get');

module.exports = function (req, res, next) {
    console.info('[routes][config][get][info] - get configuration - params: %j', req.params);

    get(req.params.id)
    .then(function (config) {
        if (!config) {
            console.info('[routes][config][get][error] - configuration not found - id: %s', req.params.id);
            return res.status(404).end();
        }

        console.info('[routes][config][get][success] - get configuration - config: %j', config);
        res.status(200).json(config);
    })
    .catch(function (error) {
        console.info('[routes][config][get][error] - get configuration - error: %j - error stack: %s', error, error.stack || 'no stack' );
        next(error);
    });
};
