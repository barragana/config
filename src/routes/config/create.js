var create  = require('../../services/config/create');

module.exports = function (req, res, next) {
    console.info('[routes][config][create][info] - create configuration - body: %j', req.body);

    create(req.body)
    .then(function (config) {
        console.info('[routes][config][create][success] - create configuration - config: %j', config);
        res.status(200).json(config);
    })
    .catch(function (error) {
        console.info('[routes][config][create][error] - create configuration - error: %j - error stack: %s', error, error.stack || 'no stack' );
        next(error);
    });
};
