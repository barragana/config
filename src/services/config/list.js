var _       = require('lodash');
var Config  = require('../../models/config');

module.exports = function (offset, limit) {
    var skip = offset ? limit * offset : 0;
    return Config.find().skip(skip).limit(limit)
    .then(function (configsList) {
        return _.map(configsList, function (config) {
            return config.toJSON();
        });
    });
};
