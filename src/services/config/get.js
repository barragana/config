var Config  = require('../../models/config');

module.exports = function (id) {

    return Config.findById(id).exec()
    .then(function (doc) {
        if (!doc) {
            return;
        }
        return doc.toJSON();
    });
};
