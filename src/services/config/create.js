var _       = require('lodash');
var Config  = require('../../models/config');

module.exports = function (data) {
    var config = new Config(data);

    var error = config.validateSync();
    if (error) {
        return global.Promise.reject(_.merge({ status: 400 }, error));
    }

    return config.save()
    .then(function (doc) {
        return doc.toJSON();
    });
};
