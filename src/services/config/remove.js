var Config  = require('../../models/config');

module.exports = function (id) {
    return Config.findById(id).exec()
    .then(function (doc) {
        if (!doc) {
            return false;
        }
        return doc.remove();
    });
};
