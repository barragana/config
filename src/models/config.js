var _            = require('lodash');
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var ConfigSchema = new Schema({
  name  : { type: String, required: true},
  token : { type: String, required: true},
  access: {
      apps          : { type: [{ type: String, required: true}], required: true},
      contexts      : { type: [{ type: String, required: true}], required: true},
      entity_types  : { type: String, required: true }
  }
});

var fields = ['name', 'token', 'access'];
ConfigSchema.set('toJSON', {
    transform: function(doc) {
        return _.pick(doc, fields);
    }
});

module.exports = mongoose.model('Config', ConfigSchema);
