# 7NXT
###### Simple API to handle cloud service configuration

To use it, simply:

##### Environment Configuration
---
config mongodb connection is based on:
```js
process.env.DB_URI
```
or config mongodb connection is based on:
```js
process.env.DB_HOST
process.env.DB_PORT
process.env.DB_NAME
```
define an authorization to access the APIs
```js
process.env.AUTHORIZATION
```
define port to run the server
```js
process.env.PORT
```
##### Run
---
1. npm start
2. npm run dev -> reload when file changes
3. npm run test
